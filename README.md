# typemap

You can create bidirectional mappings between types and enumeration values.
Refer to tests for more details and read more on [my blog](https://twdev.blog/2023/11/enum_typemap/).
