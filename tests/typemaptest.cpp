#include <typemap/typemap.hpp>

#include <string>
#include <type_traits>

enum class E {
  First,

  A,
  B,
  C,

  Last,
};

DECL_MAPPING_BEGIN(E);
DECL_MAPPING(E::A, int);
DECL_MAPPING(E::B, float);
DECL_MAPPING(E::C, std::string);
DECL_MAPPING_END(E);

static_assert(std::is_same_v<ENUM_TO_TYPE(E::A), int>, "");
static_assert(std::is_same_v<ENUM_TO_TYPE(E::B), float>, "");
static_assert(std::is_same_v<ENUM_TO_TYPE(E::C), std::string>, "");

static_assert(INDEX_TYPE(E, int) == E::A, "");
static_assert(INDEX_TYPE(E, float) == E::B, "");
static_assert(INDEX_TYPE(E, std::string) == E::C, "");

int main(int, const char *[])
{
    // This is a compile time test
    return 0;
}
