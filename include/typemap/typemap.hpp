#include <typelist/typelist.hpp>

namespace typemap {

template <typename ET, ET EV> struct PriorEnum {
  static constexpr ET value =
      EV == ET::First
          ? ET::First
          : static_cast<ET>(static_cast<std::underlying_type_t<ET>>(EV) - 1);
};

template <typename ET, ET EV>
inline constexpr ET PriorEnumV = PriorEnum<ET, EV>::value;

template <typename ET, ET EV> struct EnumToType;

template <typename ET, ET EV>
using EnumToTypeT = typename EnumToType<ET, EV>::type;

template <typename ET, ET EV>
using EnumToTypeM = typename EnumToType<ET, EV>::tmap;

#define DECL_MAPPING(ev, t)                                                    \
  template <> struct typemap::EnumToType<decltype(ev), ev> {                   \
    using enum_type = decltype(ev);                                            \
    static constexpr enum_type value = ev;                                     \
    static constexpr enum_type prior_value =                                   \
        typemap::PriorEnumV<enum_type, value>;                                 \
    using type = t;                                                            \
    using prior_tmap = typemap::EnumToTypeM<enum_type, prior_value>;           \
    using tmap = typelist::AppendT<prior_tmap, type>;                          \
  }

#define DECL_MAPPING_BEGIN(ET)                                                 \
  template <> struct typemap::EnumToType<ET, ET::First> {                      \
    using tmap = typelist::TypeList<void>;                                     \
  }

#define DECL_MAPPING_END(ET) DECL_MAPPING(ET::Last, void)

#define INDEX_TYPE(ET, t)                                                      \
  static_cast<ET>(typelist::IndexV<typemap::EnumToTypeM<ET, ET::Last>, t>)

#define ENUM_TO_TYPE(EV) typemap::EnumToTypeT<decltype(EV), EV>

} // namespace typemap
